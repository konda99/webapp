import random
import string
class Person:
    def __init__(self):
        self.name = ''.join(random.choices(string.ascii_lowercase, k=10)).capitalize()
        self.age = random.randrange(0,99)
