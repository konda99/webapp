##Miguel grinberg , flask tutorial . materialize

from pony.orm import Database, Required, Optional, db_session, select
from flask import Flask, render_template , request, jsonify, redirect, url_for
from person import Person
app = Flask(__name__)

db = Database()

class Person(db.Entity):
    name = Required(str)
    surname = Required(str)

db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)


'''
@app.route('/home')
@app.route('/home<some_name>')
def hello_world(some_name=''):
    people= [Person() for i in range(20)]
    return render_template('index.html', PEOPLE=people,SOME_NAME=some_name)

@app.route('/addperson')
def hello(some_name=''):
    return render_template('addperson.html')

x={'Rei':'Telekom'}
'''

'''
@app.route('/submit', methods=['GET','POST'])
def submit():
    if request.method == 'POST':
        x[request.form.get('name')]=request.form.get('major')
        return jsonify(x)
    else:
            name = request.args.get('search')
            if name in x:
                return x[name]
            else:
                return render_template('submit.html')
'''

"""
@app.route('/home', methods=['GET', 'POST'])
@db_session
def goodbye():

    if request.method == 'POST':
        name = request.form.get('name')
        major = request.form.get('major')

        Person(name=name, major=major)
        return f'Added a new person with name {name} and major {major}'

    else:
        name = request.args.get('search')
        person = select(p for p in Person if p.name == name).first()

        if person:
            return person.major

        else:
            return render_template('index.html')

"""




@app.route('/edrirama', methods=['GET', 'POST'])
@db_session
def tables():

    people = select(p for p in Person)

    if request.method == 'POST':
        name = request.form.get('name')
        surname = request.form.get('surname')

        Person(name=name, surname=surname)
        return redirect(url_for('.tables'))


    else:
        return render_template('table.html', PEOPLE=people)



@app.route('/delete/<id>', methods=['GET', 'POST'])
@db_session
def delete(id):
    Person[id].delete()
    return redirect(url_for('.tables'))


"""
    person = select(p for p in Person if p.name == name).first()

    if person:
        return person.name

    else:
    """
